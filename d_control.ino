// кнопки-крутилки

// void encTick() {
//   enc.tick();
//   if (enc.isTurn()) {
//     timeoutReset();
//     if (enc.isLeft()) {
//       thisVolume += 5;
//     }
//     if (enc.isRight()) {
//       thisVolume -= 5;
//     }
//     dispMode();
//   }
// }

// void btnTick() {
//   if (btn.holded()) {
//     timeoutReset();
//     workMode = !workMode;
//     dispMode();
//   }
//   if (encBtn.holded()) {
//     pumpON();
//     while (!digitalRead(ENC_SW));
//     timeoutReset();
//     pumpOFF();
//   }
// }

void encTick() {
  button1.tick();
  button2.tick();
}

void btnTick() {
  button3.tick();
}

void clickButtonUp() { // енкодер Up
  timeoutReset();
  thisVolume += 5;
  dispMode();
}

void clickButtonDown() { // енкодер Down
  timeoutReset();
  thisVolume -= 5;
  dispMode();
}

void clickButtonMain() { // главная кнопка

  if (systemON) stopPumping();  // астановка сисемы если активирована

  systemON = !systemON;    // система активирована

  if (systemState == WAIT)
    systemState = SEARCH;

  dispMode();
}

void longPressButtonMain() { // енкодер клик
  // while (!digitalRead(ENC_SW)); // пока энкодер нажат

  systemON = false;    // система активирована
  timeoutReset();
  pumpOFF();
  dispMode();
}
